const express = require('express');
const validate = require('express-validation');
const controller = require('../../controllers/client.controller');
const { authorize } = require('../../middlewares/auth');
const { list, create, replace } = require('../../validations/client.validation');

const router = express.Router();

router.param('clientId', controller.load);

router
  .route('/')
  .get(authorize(), validate(list), controller.list)
  .post(authorize(), validate(create), controller.create);


router
  .route('/:id')
  .get(authorize(), controller.get)
  .put(authorize(), validate(replace), controller.replace)
//   .patch(authorize(LOGGED_USER), validate(updateUser), controller.update)
  .delete(authorize(), controller.remove);


module.exports = router;
