const express = require('express');
const validate = require('express-validation');
const controller = require('../../controllers/notes.controller');
const { authorize, ADMIN, LOGGED_USER } = require('../../middlewares/auth');
const { listNotes, createNotes } = require('../../validations/notes.validation');

const router = express.Router();

// router.param('noteId', controller.load);

router
  .route('/')
  .get(authorize(), validate(listNotes), controller.list)
  .post(authorize(), validate(createNotes), controller.create);


// router
//   .route('/profile')
//   .get(authorize(), controller.loggedIn);


// router
//   .route('/:noteId')
//   .get(authorize(LOGGED_USER), controller.get)
//   .put(authorize(LOGGED_USER), validate(replaceUser), controller.replace)
//   .patch(authorize(LOGGED_USER), validate(updateUser), controller.update)
//   .delete(authorize(LOGGED_USER), controller.remove);


module.exports = router;
