const mongoose = require('mongoose');
const httpStatus = require('http-status');
const { omitBy, isNil } = require('lodash');
const moment = require('moment-timezone');
const APIError = require('../utils/APIError');


const noteSchema = new mongoose.Schema({
  name: {
    type: String,
  },
  secondName: {
    type: String,
  },
  thirdName: {
    type: String,
  },
  note: {
    type: String,
  },
  // date: {
  //   type: Date,
  // },
}, {
  timestamps: true,
});

/**
 * Methods
 */
noteSchema.method({
  transform() {
    const transformed = {};
    const fields = ['name', 'secondName', 'thirdName', 'note'];
    fields.forEach((field) => {
      transformed[field] = this[field];
    });
    return transformed;
  },
});

/**
 * Statics
 */
noteSchema.statics = {

  /**
   * Get note
   *
   * @param {ObjectId} id - The objectId of user.
   * @returns {Promise<User, APIError>}
   */
  async get(id) {
    try {
      let note;

      if (mongoose.Types.ObjectId.isValid(id)) {
        note = await this.findById(id).exec();
      }
      if (note) {
        return note;
      }

      throw new APIError({
        message: 'Note does not exist',
        status: httpStatus.NOT_FOUND,
      });
    } catch (error) {
      throw error;
    }
  },

  /**
   * List nodes in descending order of 'createdAt' timestamp.
   *
   * @param {number} skip - Number of users to be skipped.
   * @param {number} limit - Limit number of users to be returned.
   * @returns {Promise<User[]>}
   */
  list({
    page = 1, perPage = 30, name
  }) {
    const options = omitBy({ name }, isNil);

    return this.find(options)
      .sort({ createdAt: -1 })
      .skip(perPage * (page - 1))
      .limit(perPage)
      .exec();
  },

};

/**
 * @typedef User
 */
module.exports = mongoose.model('Note', noteSchema);
