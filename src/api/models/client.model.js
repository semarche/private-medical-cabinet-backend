const mongoose = require('mongoose');
const httpStatus = require('http-status');
// const { omitBy, isNil } = require('lodash');
const APIError = require('../utils/APIError');
// const moment = require('moment-timezone');


const schema = new mongoose.Schema({
  illness: {
    type: String,
  },
  from: {
    type: String,
  },
  fullName: {
    type: String,
  },
  profession: {
    type: String,
  },
  rating: {
    type: Number,
  },
  telephoneNumber: {
    type: Array,
  },
  age: {
    type: String,
  },
  description: {
    type: String,
  },
}, {
  timestamps: true,
});

/**
 * Methods
 */
schema.method({
  transform() {
    const transformed = {};
    const fields = ['_id', 'illness', 'from', 'fullName', 'profession',
      'rating', 'telephoneNumber', 'age', 'description'];
    fields.forEach((field) => {
      transformed[field] = this[field];
    });
    return transformed;
  },
});

/**
 * Statics
 */
schema.statics = {

  async get(id) {
    try {
      let data;
      if (mongoose.Types.ObjectId.isValid(id)) {
        data = await this.findById(id).exec();
      }
      if (data) {
        return data;
      }
      throw new APIError({
        message: 'Client does not exist',
        status: httpStatus.NOT_FOUND,
      });
    } catch (error) {
      throw error;
    }
  },

  list({
    page = 1,
    perPage = 30,
    search,
  }) {
    const options = {
      $or: [
        { illness: new RegExp(search, 'i') },
        // { description: new RegExp(search, 'i') },
        { from: new RegExp(search, 'i') },
        { fullName: new RegExp(search, 'i') },
        { telephoneNumber: new RegExp(search, 'i') },
      ],
    };

    return this.find(options)
      .sort({ createdAt: -1 })
      .skip(perPage * (page - 1))
      .limit(perPage)
      .exec();
  },

};

module.exports = mongoose.model('Client', schema);
