const mongoose = require('mongoose');
const httpStatus = require('http-status');
const { omitBy, isNil } = require('lodash');
const APIError = require('../utils/APIError');
// const moment = require('moment-timezone');


const schema = new mongoose.Schema({
  date: {
    type: Date,
  },
  clientId: {
    type: String,
  },
  clientFullName: {
    type: String,
  },
  manipulationName: {
    type: String,
  },
  cost: {
    type: Number,
  },
  description: {
    type: String,
  },
}, {
  timestamps: true,
});

/**
 * Methods
 */
schema.method({
  transform() {
    const transformed = {};
    const fields = ['_id', 'date', 'clientId', 'clientFullName', 'manipulationName', 'cost', 'description'];
    fields.forEach((field) => {
      transformed[field] = this[field];
    });
    return transformed;
  },
});

/**
 * Statics
 */
schema.statics = {

  async get(id) {
    try {
      let data;
      if (mongoose.Types.ObjectId.isValid(id)) {
        data = await this.findById(id).exec();
      }
      if (data) {
        return data;
      }
      throw new APIError({
        message: 'Manipulation does not exist',
        status: httpStatus.NOT_FOUND,
      });
    } catch (error) {
      throw error;
    }
  },

  list({
    page = 1, perPage = 30, search, clientId,
  }) {
    const options = clientId ?
      { clientId } :
      {
        $or: [
          { clientFullName: new RegExp(search, 'i') },
          { manipulationName: new RegExp(search, 'i') },
          { description: new RegExp(search, 'i') },
        ],
      };

    return this.find(options)
      .sort({ createdAt: -1 })
      .skip(perPage * (page - 1))
      .limit(perPage)
      .exec();
  },

};

module.exports = mongoose.model('Manipulation', schema);
