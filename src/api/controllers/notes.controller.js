const httpStatus = require('http-status');
const { omit } = require('lodash');
const Note = require('../models/notes.model');
const { handler: errorHandler } = require('../middlewares/error');

exports.load = async (req, res, next, id) => {
  try {
    const user = await Note.get(id);
    req.locals = { notes };
    return next();
  } catch (error) {
    return errorHandler(error, req, res);
  }
};

/**
 * Get list
 * @public
 */
exports.get = (req, res) => res.json(req.locals.notes.transform());


/**
 * Create new user
 * @public
 */
exports.create = async (req, res, next) => {
  try {
    const note = new Note(req.body);
    const savedNote = await note.save();
    res.status(httpStatus.CREATED);
    res.json(savedNote.transform());
  } catch (error) {
    next(error);
  }
};

/**
 * Replace existing user
 * @public
 */
// exports.replace = async (req, res, next) => {
//   try {
//     const { user } = req.locals;
//     const newUser = new User(req.body);
//     const ommitRole = user.role !== 'admin' ? 'role' : '';
//     const newUserObject = omit(newUser.toObject(), '_id', ommitRole);
//
//     await user.update(newUserObject, { override: true, upsert: true });
//     const savedUser = await User.findById(user._id);
//
//     res.json(savedUser.transform());
//   } catch (error) {
//     next(User.checkDuplicateEmail(error));
//   }
// };

/**
 * Update existing user
 * @public
 */
// exports.update = (req, res, next) => {
//   const ommitRole = req.locals.user.role !== 'admin' ? 'role' : '';
//   const updatedUser = omit(req.body, ommitRole);
//   const user = Object.assign(req.locals.user, updatedUser);
//
//   user.save()
//     .then(savedUser => res.json(savedUser.transform()))
//     .catch(e => next(User.checkDuplicateEmail(e)));
// };

/**
 * Get user list
 * @public
 */
exports.list = async (req, res, next) => {
  try {
    const notes = await Note.list(req.query);
    const transformedNotes = notes.map(note => note.transform());
    res.json(transformedNotes);
  } catch (error) {
    next(error);
  }
};

/**
 * Delete user
 * @public
 */
// exports.remove = (req, res, next) => {
//   const { user } = req.locals;
//
//   user.remove()
//     .then(() => res.status(httpStatus.NO_CONTENT).end())
//     .catch(e => next(e));
// };
