const httpStatus = require('http-status');
const { omit } = require('lodash');
const Manipulation = require('../models/manipulation.model');
const { handler: errorHandler } = require('../middlewares/error');

exports.load = async (req, res, next, id) => {
  try {
    const data = await Manipulation.get(id);
    req.locals = { manipulation: data };
    return next();
  } catch (error) {
    return errorHandler(error, req, res);
  }
};

exports.create = async (req, res, next) => {
  try {
    const data = new Manipulation(req.body);
    const savedData = await data.save();
    res.status(httpStatus.CREATED);
    res.json(savedData.transform());
  } catch (error) {
    next(error);
  }
};

// };
//
// /**
//  * Update existing user
//  * @public
//  */
// exports.update = (req, res, next) => {
//   const ommitRole = req.locals.user.role !== 'admin' ? 'role' : '';
//   const updatedUser = omit(req.body, ommitRole);
//   const user = Object.assign(req.locals.user, updatedUser);
//
//   user.save()
//     .then(savedUser => res.json(savedUser.transform()))
//     .catch(e => next(User.checkDuplicateEmail(e)));
// };

exports.replace = async (req, res, next) => {
  try {
    const savedData = await Manipulation.findOneAndUpdate({ _id: req.params.id }, req.body, { upsert: true });
    res.status(httpStatus.ACCEPTED);
    res.json(savedData.transform());
  } catch (error) {
    next(error);
  }
};

exports.list = async (req, res, next) => {
  try {
    const data = await Manipulation.list(req.query);
    const transformedData = data.map(note => note.transform());
    res.json(transformedData);
  } catch (error) {
    next(error);
  }
};

exports.get = async (req, res, next) => {
  try {
    const data = await Manipulation.get(req.params.id);
    res.json(data.transform());
  } catch (error) {
    next(error);
  }
};

exports.remove = (req, res, next) => {
  Manipulation.findOneAndRemove({ _id: req.params.id })
    .then(() => res.status(httpStatus.NO_CONTENT).end())
    .catch(e => next(e));
};
