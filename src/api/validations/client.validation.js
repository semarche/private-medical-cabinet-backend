const Joi = require('joi');
const Note = require('../models/notes.model');

module.exports = {
  // GET /v1/client
  list: {
    query: {
      search: Joi.string(),
      page: Joi.number().min(1),
      perPage: Joi.number().min(1).max(100),
      illness: Joi.string(),
      from: Joi.string(),
      fullName: Joi.string(),
      profession: Joi.string(),
      rating: Joi.number(),
      telephoneNumber: Joi.array(),
      age: Joi.string(),
      description: Joi.string(),
    },
  },

  // POST /v1/client
  create: {
    body: {
      illness: Joi.string().allow(''),
      from: Joi.string().allow(''),
      fullName: Joi.string().required(),
      profession: Joi.string().allow(''),
      rating: Joi.number(),
      telephoneNumber: Joi.array(),
      age: Joi.string().allow(''),
      description: Joi.string().allow(''),
    },
  },

  // PUT /v1/client/:id
  replace: {
    body: {
      illness: Joi.string().allow(''),
      from: Joi.string().allow(''),
      fullName: Joi.string().required(),
      profession: Joi.string().allow(''),
      rating: Joi.number(),
      telephoneNumber: Joi.array(),
      age: Joi.string().allow(''),
      description: Joi.string().allow(''),
    },
  },

  // PATCH /v1/users/:userId
  // updateUser: {
  //   body: {
  //     email: Joi.string().email(),
  //     password: Joi.string().min(6).max(128),
  //     name: Joi.string().max(128),
  //     role: Joi.string().valid(User.roles),
  //   },
  //   params: {
  //     userId: Joi.string().regex(/^[a-fA-F0-9]{24}$/).required(),
  //   },
  // },
};
