const Joi = require('joi');
const Note = require('../models/notes.model');

module.exports = {
  // GET /v1/manipulation
  list: {
    query: {
      search: Joi.string(),
      page: Joi.number().min(1),
      perPage: Joi.number().min(1).max(100),
      date: Joi.date(),
      clientId: Joi.string(),
      clientFullName: Joi.string(),
      manipulationName: Joi.string(),
      cost: Joi.number(),
      description: Joi.string(),
    },
  },

  // POST /v1/manipulation
  create: {
    body: {
      date: Joi.date(),
      clientId: Joi.string().required(),
      clientFullName: Joi.string().required(),
      manipulationName: Joi.string().required(),
      cost: Joi.number(),
      description: Joi.string().allow(''),
    },
  },

  // PUT /v1/manipulation/:id
  replace: {
    body: {
      date: Joi.date(),
      clientId: Joi.string().required(),
      clientFullName: Joi.string().required(),
      manipulationName: Joi.string().required(),
      cost: Joi.number(),
      description: Joi.string().allow(''),
    },
  },

  // PATCH /v1/users/:userId
  // updateUser: {
  //   body: {
  //     email: Joi.string().email(),
  //     password: Joi.string().min(6).max(128),
  //     name: Joi.string().max(128),
  //     role: Joi.string().valid(User.roles),
  //   },
  //   params: {
  //     userId: Joi.string().regex(/^[a-fA-F0-9]{24}$/).required(),
  //   },
  // },
};
