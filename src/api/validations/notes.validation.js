const Joi = require('joi');
const Note = require('../models/notes.model');

module.exports = {

  // GET /v1/users
  listNotes: {
    query: {
      page: Joi.number().min(1),
      perPage: Joi.number().min(1).max(100),
      secondName: Joi.string(),
      thirdName: Joi.string(),
      note: Joi.string(),
    },
  },

  // POST /v1/users
  createNotes: {
    body: {
      name: Joi.string(),
      secondName: Joi.string(),
      thirdName: Joi.string(),
      note: Joi.string(),
    },
  },

  // PUT /v1/users/:userId
  // replaceUser: {
  //   body: {
  //     email: Joi.string().email().required(),
  //     password: Joi.string().min(6).max(128).required(),
  //     name: Joi.string().max(128),
  //     role: Joi.string().valid(User.roles),
  //   },
  //   params: {
  //     userId: Joi.string().regex(/^[a-fA-F0-9]{24}$/).required(),
  //   },
  // },

  // PATCH /v1/users/:userId
  // updateUser: {
  //   body: {
  //     email: Joi.string().email(),
  //     password: Joi.string().min(6).max(128),
  //     name: Joi.string().max(128),
  //     role: Joi.string().valid(User.roles),
  //   },
  //   params: {
  //     userId: Joi.string().regex(/^[a-fA-F0-9]{24}$/).required(),
  //   },
  // },
};
