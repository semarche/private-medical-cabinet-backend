FROM node:8-alpine
MAINTAINER Marchenko Sergey <sergey.dnepro@gmail.com>

EXPOSE 4000

ARG NODE_ENV
ENV NODE_ENV $NODE_ENV

RUN mkdir -p /app
WORKDIR /app

ADD package.json /app/
RUN npm install --no-optional  --silent

ADD ./ /app

CMD ["npm", "run", "prod"]
