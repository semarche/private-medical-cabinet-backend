backend part for https://gitlab.com/semarche/private-medical-cabinet-front

The project is under development

how to run:

*project require mongoDB

git clone https://gitlab.com/semarche/private-medical-cabinet-backend.git<br />
cd private-medical-cabinet-backend<br />
npm install<br />
npm run dev<br />

or you can use Docker for build image and start app inside container:<br />
docker build -t private-medical-cabinet-backend .<br />
docker run -d -p 4000:4000 --name pmcb_container private-medical-cabinet-backend

or you can use already built Docker image<br />
docker pull registry.gitlab.com/semarche/private-medical-cabinet-backend<br />
docker run -d -p 4000:4000 --name pmcb_container registry.gitlab.com/semarche/private-medical-cabinet-backend

